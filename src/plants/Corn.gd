extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func _init():
	self.icon = preload("res://assets/icon corn.png")
	self.icon_tooltip = "Corn needs lots of nutrients and light, but can be very productive"
	self.description = "A staple crop that grows well in sun"
	self.light_range = [25, 98]
	self.sprites[3] = preload("res://assets/corn fruit.png")
	self.sprites[4] = preload("res://assets/corn ripe.png")
	self.sprites[5] = preload("res://assets/corn ripe.png")
	self.heights = [0, 1, 2, 3, 3, 3]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

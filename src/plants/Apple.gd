extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init():
	self.icon = preload("res://assets/apple icon.png")
	self.icon_tooltip = "A slow growing tree that survives over winter"
	self.description = "Delicious, but you have to plan ahead"
	self.light_range = [25, 75]
	self.season_nutrient_mod = -1
	self.season_health_mod = -1
	self.harvest_nutrient_mod = 0
	self.harvest_health_mod = 0
	self.base_score = 300
	self.sprites[2] = preload("res://assets/apple plant.png")
	self.sprites[3] = preload("res://assets/apple plant.png")
	self.sprites[4] = preload("res://assets/apple tree.png")
	self.sprites[5] = preload("res://assets/apple fruit.png")
	self.heights = [0, 1, 2, 3, 4, 5]
	self.score_mod_by_growth = [0.0, 0.0, 0.0, 0.0, 2, 3]
	self.growth_thresholds = [200, 400, 600, 800, 1000]
	self.survives_winter = true
	self.harvest_rockiness_mod = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

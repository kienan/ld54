extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init():
	self.icon = preload("res://assets/bean icon.png")
	self.icon_tooltip = "Beans help replenish nutrients of neighbouring tiles"
	self.description = "A staple that is good at replenishing soil nutrients"
	self.light_range = [15, 75]
	self.base_score = 75
	self.season_nutrient_mod = -5
	self.harvest_nutrient_mod = 50
	self.harvest_health_mod = 0
	self.sprites[3] = preload("res://assets/bean plant.png")
	self.sprites[4] = preload("res://assets/bean plant.png")
	self.sprites[5] = preload("res://assets/bean plant.png")
	self.heights = [0, 1, 2, 2, 2, 3]


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

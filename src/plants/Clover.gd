extends "res://src/Plant.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init():
	self.icon = preload("res://assets/clover icon.png")
	self.icon_tooltip = "Clover helps replenish the health of the soil on the tile"
	self.description = "Good for the soil, and bees like it too"
	self.light_range = [10, 90]
	self.season_nutrient_mod = -1
	self.season_health_mod = 5
	self.harvest_health_mod = 10
	self.harvest_rockiness_mod = 0
	self.base_score = 0
	self.sprites[2] = preload("res://assets/clover plant.png")
	self.sprites[3] = preload("res://assets/clover plant.png")
	self.sprites[4] = preload("res://assets/clover flower.png")
	self.sprites[5] = preload("res://assets/clover flower.png")
	self.heights = [0, 1, 1, 1, 1, 1]
	self.growth_thresholds = [25, 50, 75, 100, 110, 125]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

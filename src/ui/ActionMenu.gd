extends VBoxContainer

const FadingText = preload("res://src/ui/FadingText.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal action_changed(new_value)

# Called when the node enters the scene tree for the first time.
func _ready():
	for c in get_node("Grid").get_children():
		c.connect("toggled", self, "_on_ActionButton_toggled", [c.get_name().to_lower()])
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ActionButton_toggled(pressed, action_name):
	var another_pressed = false
	for child in get_node("Grid").get_children():
		if child.get_name().to_lower() != action_name:
			if child.is_pressed():
				another_pressed = true
				child.set_pressed(false)
				break;
	if not another_pressed and not pressed:
		for child in get_node("Grid").get_children():
			if child.get_name().to_lower() == action_name:
				child.set_pressed(true)
				return
	if pressed:
		emit_signal("action_changed", action_name)

func _on_PassTime_pressed():
	get_node("PassTime").set_disabled(true)
	for c in get_node("Grid").get_children():
		c.set_disabled(true)
	emit_signal("action_changed", "pass_time")


func unlock():
	get_node("PassTime").set_disabled(false)
	for c in get_node("Grid").get_children():
		c.set_disabled(false)

func _on_action_points_changed(old, new):
	get_node("ActionPoints").set_text("Action Points: %d" % new)
	if new > old:
		var f = FadingText.instance()
		f.set_text("%+d AP" % (new - old))
		f.set_modulate(Color(0.0, 0.9, 0.1))
		add_child(f)

extends VBoxContainer

signal plant_type_changed(name)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func set_plants(plant_types):
	var i = 1
	for x in plant_types:
		if not plant_types[x].plantable and false:
			continue
		var button = Button.new()
		#button.set_normal_texture(plant_types[x].icon)
		button.set_button_icon(plant_types[x].icon)
		button.set_toggle_mode(true)
		if i <= 10:
			var short = ShortCut.new()
			var ip = InputEventKey.new()
			ip.set_scancode(KEY_0 + (i % 10))
			short.set_shortcut(ip)
			button.set_shortcut(short)
		button.connect("toggled", self, "_on_plant_icon_toggled", [button, x])
		get_node("Plants").add_child(button)
		button.set_tooltip(plant_types[x].get_icon_tooltip())
		# Drag and drop
		if i == 1:
			self._on_plant_icon_toggled(true, button, x)
		i += 1
		
func _on_plant_icon_toggled(pressed, button, plant_type_name):
	var another_pressed = false
	for child in get_node("Plants").get_children():
		if child != button:
			if child.is_pressed():
				another_pressed = true
				break;
	if not another_pressed and not pressed:
		button.set_pressed(true)
		return
	if pressed:
		for child in get_node("Plants").get_children():
			if child == button:
				continue
			child.set_pressed(false)
	else:
		get_node("Details").set_bbcode("")
	if (pressed):
		var game = get_tree().current_scene
		if game.plant_types[plant_type_name] != null:
			get_node("Details").set_bbcode("%s\n\n%s" %[plant_type_name.capitalize(), game.plant_types[plant_type_name].get_description()])
		emit_signal("plant_type_changed", plant_type_name)
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


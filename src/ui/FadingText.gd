extends RichTextLabel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var life_max = 1.5
var life_cur = life_max
var autostart = true
var started = false
var move_towards = false
var move_speed = 10

func get_text_size():
	var f = self.get_theme_default_font()
	return f.get_string_size(self.get_text())

# Called when the node enters the scene tree for the first time.
func _ready():
	if self.autostart:
		self.started = true
	print(self.get_position())

func _process(delta):
	if not started:
		return
	self.life_cur -= delta
	if self.life_cur <= 0.0:
		self.set_visible(false)
		self.started = false
		self.queue_free()
	else:
		var m = self.get_modulate()
		var a = self.life_cur / self.life_max
		m = Color(m.r, m.g, m.b, a)
		self.set_modulate(m)
	if self.move_towards is Vector2:
		var new_pos = self.get_position().move_toward(self.move_towards, delta * self.move_speed)
		self.set_position(new_pos)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

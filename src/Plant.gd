extends Node2D


const FadingText = preload("res://src/ui/FadingText.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum STAGE {SEED, SPROUT, GROWTH, FRUIT, RIPE}
const default_sprites = [
	preload("res://assets/seeds.png"),
	preload("res://assets/sprouts.png"),
	preload("res://assets/plant.png"),
	preload("res://assets/plant.png"),
	preload("res://assets/plant.png"),
	preload("res://assets/plant.png")
]
var sprites = []
var icon = preload("res://assets/hex.png")
var icon_tooltip = "Plaaaant me"
var description = "A plant"
# run time
var growth = 0
var height = 0

# per-type stats
# each stat contributes to growth during each section of the day
# there are 4 sections to the day, and 4 growing periods
var light_range = [15, 85] # below x too shady, above y too sunny
var growth_mod_by_light = [2, 10, -1] # shady, ok, sunny

var rockiness_range = [5, 50]
var growth_mod_by_rockiness = [-1, 10, -5]

var nutrient_range = [10, 100]
var growth_mod_by_nutrition = [0, 10, 15]

var soil_health_range = [25, 75]
var growth_mod_by_soil_health = [0, 5, 10]

var base_score = 100
var score_mod_by_growth = [0.0, 0.0, 0.0, 0.1, 1.2, 1.5]
var growth_thresholds = [100, 175, 250, 300, 400] # for each stage above
var heights = [0, 0, 1, 1, 1, 1]

var plantable = true
var survives_winter = false
var season_rockiness_mod = 0
var season_nutrient_mod = -10
var season_health_mod = -5
var harvest_nutrient_mod = 5
var harvest_health_mod = 5
var harvest_rockiness_mod = 5

# @TODO adjacency effects

static func find_by_range_area(value, ranges, mods):
	var v = 0
	for x in ranges:
		if value > x:
			v += 1
			continue
		break
	if v >= mods.size():
		v = mods.size()-1
	return mods[v]

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Sprite").set_texture(self.sprites[self.get_stage()])

func get_description():
	var s = "%s\n\n" % self.description
	s += "Base score: %d\nScore mods by growth stage: %s\n\n" % [self.base_score, self.score_mod_by_growth]
	s += "At the end of each season\n\tRockiness: %d\n\tSoil health: %d\n\tSoil nutrients: %d\n\n" %[self.season_rockiness_mod, self.season_health_mod, self.season_nutrient_mod]
	s += "When harvested:\n\tRockiness: %d\n\tSoil health: %d\n\tSoil nutrients: %d\n" %[self.harvest_rockiness_mod, self.harvest_health_mod, self.harvest_nutrient_mod]
	return s

func _init():
	self.sprites.resize(self.default_sprites.size())
	for x in range(self.default_sprites.size()):
		if self.sprites[x] == null:
			self.sprites[x] = self.default_sprites[x]

func query_details():
	var stage = self.get_stage()
	var next = "N/A"
	if stage < self.growth_thresholds.size():
		next = str(self.growth_thresholds[stage])
	var s = "Growth: %d\nHeight: %d\nNext stage at: %s\n\n" % [self.growth, self.height, next]
	return s

func get_stage():
	var stage = 0
	for x in self.growth_thresholds:
		if self.growth >= x:
			stage += 1
		else:
			break
	return stage

func get_icon_tooltip():
	return self.icon_tooltip
	
func apply_light(light, soil):
	var growth_light = self.find_by_range_area(light, self.light_range, self.growth_mod_by_light)
	var growth_rockiness = self.find_by_range_area(soil.rockiness, self.rockiness_range, self.growth_mod_by_rockiness)
	var growth_health = self.find_by_range_area(soil.misc_health, self.soil_health_range, self.growth_mod_by_soil_health)
	var growth_nutrients = self.find_by_range_area(soil.nutrients, self.nutrient_range, self.growth_mod_by_nutrition)
	var growth = growth_light + growth_rockiness + growth_health + growth_nutrients
	#print("Grew %d = %d + %d + %d + %d" %[growth, growth_light, growth_rockiness, growth_health, growth_nutrients])
	self.grow(growth)

func grow(value):
	var index = self.get_stage()
	if index >= self.heights.size():
		index = self.heights.size() - 1
	self.growth += value
	self.height = self.heights[index]
	get_node("Height").set_text(str(self.height))
	get_node("Sprite").set_texture(self.sprites[index])
	var t = FadingText.instance()
	t.set_position(Vector2(32, -24))
	t.move_towards = Vector2(50, -60)
	t.set_text("%+d🌱🌱" % value)
	t.set_modulate(Color(0.1, 0.85, 0.1))
	t.set_scale(Vector2(1.5, 1.5))
	add_child(t)

func get_score():
	var mod = self.find_by_range_area(self.growth, self.growth_thresholds, self.score_mod_by_growth)
	var score = int(round(float(self.base_score) * mod))
	return score

func use_nutrients_and_run_plant_effects(soil, old_season, new_season, grid):
	print("plant use nitrients ", self)
	soil.update_stat("rockiness", self.season_rockiness_mod)
	soil.update_stat("nutrients", self.season_nutrient_mod)
	soil.update_stat("health", self.season_health_mod)

func end_of_year(soil, grid):
	soil.update_stat("nutrients", self.harvest_nutrient_mod)
	soil.update_stat("health", self.harvest_health_mod)
	if not self.survives_winter:
		soil.update_stat("rockiness", self.harvest_rockiness_mod)
		self.queue_free()
		soil.stored = null
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

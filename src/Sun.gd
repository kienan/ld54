extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var goal = 0.0
var goals = [0.0, 0.35, 0.75, 1.0]
var speed = 0.3
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_day_time_changed(old, new):
	#print("Sun time: ", new)
	self.goal = goals[new]

func _process(delta):
	if abs(self.goal - get_node("Path2D/PathFollow2D").get_unit_offset()) > 0.1:
		var offset = get_node("Path2D/PathFollow2D").get_unit_offset()
		offset += self.speed * delta
		if offset >= self.goal:
			offset = self.goal
		get_node("Path2D/PathFollow2D").set_unit_offset(offset)

extends "res://src/Hex.gd"

const Plant = preload("res://src/Plant.tscn")

signal soil_stat_changed(name, old, new)
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var weed_rockiness_range = [20, 60]
var weed_probability_from_rockiness = [0.05, 0.1, 0.15]
var weed_health_range = [25, 50, 75]
var weed_probability_from_health = [0.2, 0.15, 0.1, 0.05]
var weed_nutrient_range = [33, 66]
var weed_probability_from_nutrient = [0.1, 0.2, 0.3]
var strong_weed_probability = 0.5

var rockiness = 0
var nutrients = 50
var misc_health = 50 # pH, moisture, aeration

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

static func find_by_range_area(value, ranges, mods):
	var v = 0
	for x in ranges:
		if value > x:
			v += 1
			continue
		break
	if v >= mods.size():
		v = mods.size()-1
	return mods[v]

func can_do_action(action):
	if action == "plant":
		return [true, self.stored == null, "", 0]
	elif action == "cull":
		return [true, self.stored != null, "You can only cull on soil tiles that have plants", 1]
	elif action == "burn":
		return [true, self.stored != null, "You can only burn soil tiles that have plants", 2]
	elif action == "removerock":
		return [true, self.rockiness >= 20, "You can only remove rocks on soil that have rocks >= 20", 3]
	elif action == "fertilize":
		return [true, true, "", 4]
	return .can_do_action(action)

func query_details():
	var details = "Rockiness: %d\nNutrients %d\nHealth %d\n\nPlant: %s\n" % [self.rockiness, self.nutrients, self.misc_health, str(self.stored)]
	if self.stored != null and self.stored:
		details += self.stored.query_details()
	details += "Soil gains rockiness and loses nutrients and health over time based on which plants are sown.\n\n"
	details += "If there are no plants, there's a chance that weeds grow.\n\n"
	details += .query_details()
	return details

func do_action(action, args):
	if action == "cull":
		assert(self.stored != null)
		var plant = self.stored
		self.stored = null
		var score = plant.get_score()
		self.update_stat("rockiness", plant.harvest_rockiness_mod)
		# @TODO do plant removal effects
		plant.queue_free()
		return score
	elif action == "plant":
		self.stored = args["plant"]
		add_child(self.stored)
		self.stored.add_to_group("runtime")
	elif action == "burn":
		assert(self.stored != null)
		var plant = self.stored
		self.stored = null
		self.update_stat("nutrients", 15)
		self.update_stat("health", -5)
		plant.queue_free()
	elif action == "removerock":
		self.update_stat("rockiness", -self.rockiness)
	elif action == "fertilize":
		self.update_stat("nutrients", 50)
		self.update_stat("health", 50)

func apply_light(value):
	.apply_light(value)
	if self.stored != null:
		self.stored.apply_light(value, self)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func nourish_plants_and_run_plant_effects(old_season, new_season, grid):
	print("soil nourish")
	if self.stored:
		self.stored.use_nutrients_and_run_plant_effects(self, old_season, new_season, grid)
	else:
		# Increase the health of the soil slightly from not being used
		self.update_stat("misc_health", 2)
		# Chance of generating a weed
		var p_weed = 0.0
		p_weed += find_by_range_area(self.rockiness, self.weed_rockiness_range, self.weed_probability_from_rockiness)
		p_weed += find_by_range_area(self.nutrients, self.weed_nutrient_range, self.weed_probability_from_nutrient)
		p_weed += find_by_range_area(self.misc_health, self.weed_health_range, self.weed_probability_from_health)
		if randf() <= p_weed:
			var weed_name = "weed"
			if randf() > self.strong_weed_probability:
				weed_name = "strong_weed"
			var game = get_tree().current_scene
			self.do_action("plant", {"plant": game.plant_types[weed_name].duplicate()})
			

func end_of_year(grid):
	if self.stored:
		self.stored.end_of_year(self, grid)
	#

func update_stat(name, value):
	var old
	var new
	if name == "rockiness":
		old  = self.rockiness
		new = self.rockiness + value
		new = min(100, max(0, new))
		self.rockiness = new
		var rock_sprite = null
		if self.rockiness >= 75:
			rock_sprite = load("res://assets/rocky 3.png")
		elif self.rockiness >= 50:
			rock_sprite = load("res://assets/rocky 2.png")
		elif self.rockiness >= 20:
			rock_sprite = load("res://assets/rocky 1.png")
		if rock_sprite != null:
			get_node("Rocks").set_texture(rock_sprite)
		get_node("Rocks").set_visible(rock_sprite != null)
	elif name == "health":
		old  = self.misc_health
		new = self.misc_health + value
		new = min(100, max(0, new))
		self.misc_health = new
	elif name == "nutrients":
		old  = self.nutrients
		new = self.nutrients + value
		new = min(100, max(0, new))
		self.nutrients = new
	else:
		print("Unknown stat: %s" % name)
		return
	if old != new:
		emit_signal("soil_stat_changed", name, old, new)

func get_default_modulate():
	if self.misc_health >= 75:
		return Color("#b27308")
	elif self.misc_health >= 50:
		return Color("#da8d09")
	elif self.misc_health >= 25:
		return Color("#c49911")
	return Color("#faaf2f")

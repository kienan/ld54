extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal day_time_changed(old, new)
signal season_changed(old, new)
signal year_changed(old, new)
signal action_points_changed(old, new)
signal scores_updated(new)

enum SEASON {SPRING, SUMMER_1, SUMMER_2, FALL}
const season_text = ["Spring", "Early Summer", "Late Summer", "Autumn"]
const season_light_mod = [1.0, 1.1, 1.2, 1.0]
enum TIME {DAWN, MORNING, AFTERNOON, DUSK}
const time_text = ["Dawn", "Morning", "Afternoon", "Dusk"]
const time_light_strength = [20, 60, 80, 10]
const time_light_from = [
	# q-r space
	Vector2(-1, 0),
	Vector2(0, -1),
	Vector2(1, -1),
	Vector2(1, 0)
]
const time_light_to = [
	# q-r space
	Vector2(1, 0),
	Vector2(0, 1),
	Vector2(-1, 1),
	Vector2(-1, 0)
]

const StructureSpot = preload("Spot.tscn")
const Soil = preload("Soil.tscn")
const Plant = preload("Plant.tscn")
const HexGrid = preload("HexGrid.tscn")
const FadingText = preload("res://src/ui/FadingText.tscn")

const hex_scale = 1.5
const hex_size = 32 * hex_scale
const hex_width = hex_size * sqrt(3)
const hex_height = 2 * hex_size

var year = 0
var scores = []
var current_season = SEASON.SPRING
var current_time = TIME.DAWN
var grid = null
var action_points = 3
var current_action = "plant"
var last_action = "plant"
var current_plant = "corn"
var current_ap = 3
var current_hex = null
var hex_click_start = null
var stage_timer = 0.0
var stage_time = 2.0

var plant_types = {
	"corn": preload("res://src/plants/Corn.tscn").instance(),
	"bean": preload("res://src/plants/Bean.tscn").instance(),
	"apple": preload("res://src/plants/Apple.tscn").instance(),
	"clover": preload("res://src/plants/Clover.tscn").instance(),
	"weed": preload("res://src/plants/Weed.tscn").instance(),
	"strong_weed": preload("res://src/plants/StrongWeed.tscn").instance(),
}

func sun_strength(time, season):
	return round(self.time_light_strength[time] * self.season_light_mod[season])

func update_action_points(value):
	emit_signal("action_points_changed", self.action_points, self.action_points + value)
	self.action_points += value

func reset():
	get_tree().call_group("runtime", "queue_free")
	self.year = 0
	self.scores = [0]
	self.action_points = 3
	self.update_action_points(0)
	self.current_season = SEASON.SPRING
	self.current_time = TIME.DAWN
	self.current_ap = 3
	self.current_hex = null
	if self.current_action == "pass_time":
		self.current_action = self.last_action
		get_node("Ui/Right/VBoxContainer2/ActionMenu").unlock()
	self.hex_click_start = null
	self.stage_timer = -1.0
	self.grid = null
	print("Reset")
	self.init_playfield()

func init_playfield():
	var grid = HexGrid.instance()
	grid.add_to_group("runtime")
	grid.init_storage(4)
	var center = get_viewport().get_visible_rect().size / 2
	var hex_offset = Vector2(
		0, # ((4*2)-1) * hex_width / 2,
		((4*2)-1) * hex_height / 2
	)
	hex_offset.x += center.x
	# print(str(center))
	var soil = 0
	var spot = 0
	for q in range(-3, 4):
		for r in range(-3, 4):
			var s = -q-r
			# We don't want some parts of the rhombus on the playfield
			if abs((q + r)) >= 4:
				continue
			var x = null
			var pos = Vector2(0, 0)
			if abs(q) < 3 and abs(r) < 3 and abs(s) < 3:
				# On the inside make Soil
				x = Soil.instance()
				soil +=  1
			else:
				# disable structure spots for the moment
				continue
				# On the outer ring, make a structure spot, except on the
				# bottom half
				if r >= 0:
					continue
				x = StructureSpot.instance()
				spot += 1
			x.add_to_group("runtime")
			x.set_location(Vector2(q, r))
			grid.set_item(q, r, x)
			# print("Created at ", q, ",", r, ": ", x)
			# Set position
			pos.y = hex_offset.y + (r * hex_height * 0.75)
			pos.x = hex_offset.x + (q * hex_width) + (r * (hex_size-5))
			#print(str(q), ",", str(r), " ", str(pos))
			x.set_position(pos)
			x.connect("mouse_entered", self, "_on_HexHover", [x, true])
			x.connect("mouse_exited", self, "_on_HexHover", [x, false])
			x.connect("input_event", self, "_on_HexInputEvent", [x])
			add_child(x)
	for t in range(self.time_light_from.size()):
		grid.tag_edge_nodes(self.time_light_from[t], "edge_time_%d" % t)
	#print("Created %d soil and %d spot tiles" % [soil, spot])
	self.grid = grid
	add_child(grid)

# Called when the node enters the scene tree for the first time.
func _ready():
	find_node("PlantMenu").set_plants(self.plant_types)
	get_node("Ui/Right/VBoxContainer2/ActionMenu").connect("action_changed", self, "_on_action_changed")
	self.connect("action_points_changed", get_node("Ui/Right/VBoxContainer2/ActionMenu"), "_on_action_points_changed")
	get_node("Ui/Left/VBoxContainer/PlantMenu").connect("plant_type_changed", self, "_on_plant_type_changed")
	self.connect("action_points_changed", self, "_on_action_points_changed")
	self.connect("day_time_changed", get_node("Sun"), "_on_day_time_changed")
	self.connect("day_time_changed", self, "_on_day_time_changed")
	self.connect("season_changed", self, "_on_season_changed")
	self.connect("year_changed", self, "_on_year_changed")
	self.connect("scores_updated", get_node("Ui/Menu"), "_on_scores_updated")
	self.reset()
	randomize()

func advance_year():
	emit_signal("year_changed", self.year, self.year + 1)
	self.year += 1
	self.scores.append(0)
	emit_signal("scores_updated", self.scores)

func advance_season():
	var t = self.current_season + 1
	var advance_year = false
	if t > SEASON.FALL:
		t = SEASON.SPRING
		advance_year = true
	# Do events for the end of the current season
	var banner = FadingText.instance()
	banner.set_text(self.season_text[t])
	banner.set_scale(Vector2(3.0, 3.0))
	banner.life_cur = 4.0
	var center = get_viewport().get_visible_rect().size / 2
	# Do this to remove some of the visual line splitting
	var size = banner.get_text_size() * banner.get_scale()
	#print("Banner text size: ", size)
	#print("Banner size: ", banner.get_size())
	banner.set_size(Vector2(size.x, size.y))
	size = banner.get_text_size() * banner.get_scale()
	banner.set_position(Vector2(center.x - size.x/2.0, 30 - min(10, size.y/2.0)))
	#print("Banner position: ", banner.get_position())
	
	add_child(banner)
	# Plants consume the nutrients and health of soil + Plant effects
	#for soil in get_tree().get_nodes_in_group("soil"):
	#	soil.nourish_plants_and_run_plant_effects(self.current_season, t, self.grid)
	get_tree().call_group("soil", "nourish_plants_and_run_plant_effects", self.current_season, t, self.grid)
	# Add action points
	self.update_action_points(2)
	if advance_year:
		for soil in get_tree().get_nodes_in_group("soil"):
			if soil.stored != null:
				var plant_score = soil.stored.get_score()
				self.scores[self.year] += plant_score
				if plant_score > 0:
					var f = FadingText.instance()
					f.set_text("%+d points" % plant_score)
					f.set_position(Vector2(32, 32))
					f.set_modulate(Color(0.9, 0.1, 0.9))
					soil.add_child(f)
			soil.end_of_year(self.grid)
		self.advance_year()
	self.set_season(t)
	# Any events at the start of the new season

func set_season(season: int):
	emit_signal("season_changed", self.current_season, season)
	self.current_season = season

func advance_time():
	var t = self.current_time + 1
	var advance_season = false
	print("Advancing time to ", t)
	if t > TIME.DUSK:
		t = TIME.DAWN
		advance_season = true
	# Do the things that have to be done for the old time
	# Show where light is coming from
	var group = "edge_time_%d" % self.current_time
	var sun = self.sun_strength(self.current_time, self.current_season)
	for node in get_tree().get_nodes_in_group(group):
		node.display_arrow(self.time_light_from[self.current_time],
			self.hex_height, self.hex_width, self.hex_size)
		# Calculate the light that reaches all the tiles
		var first = node
		var second = null
		var light = sun
		var last_tallest = 0
		while first != null:
			first.apply_light(light)
			last_tallest = max(last_tallest, first.get_height())
			var loc = first.pf_location + self.time_light_to[self.current_time]
			if self.grid._is_valid_q_r(loc.x, loc.y):
				second = self.grid.get_item(loc.x, loc.y)
				if second != null:
					if last_tallest > second.get_height():
						light = 0
					else:
						light = sun
						last_tallest = max(last_tallest, second.get_height())
			first = second
			second = null
			
	# Run growth on tiles
	# For tiles that have no plants, weed maybe
	if advance_season:
		self.advance_season()
	self.set_time(t)
	# Any events at the start of the new time

func set_time(period: int):
	emit_signal("day_time_changed", self.current_time, period)
	self.current_time = period
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.current_action == "pass_time":
		if self.stage_timer > 0.0:
			self.stage_timer -= delta
		else:
			self.advance_time()
			if self.current_time != TIME.DAWN:
				self.stage_timer = self.stage_time
			else:
				self.current_action = self.last_action
				self.update_current_hex_action_possibility()
				# Some other things...
				get_node("Ui/Right/VBoxContainer2/ActionMenu").unlock()
				

func _unhandled_input(event):
	if Input.is_action_pressed("ui_cancel"):
		get_node("Ui/Menu").popup_centered()
	if Input.is_action_pressed("ui_help"):
		var help = get_node("Ui/Help")
		if help.is_visible():
			help.set_visible(false)
		else:
			help.popup_centered()
	var action_menu = get_node("Ui/Right/VBoxContainer2/ActionMenu")
	if Input.is_action_pressed("ui_plant"):
		action_menu.find_node("Plant").set_pressed(true)
	if Input.is_action_pressed("ui_query"):
		action_menu.find_node("Query").set_pressed(true)
	if Input.is_action_pressed("ui_cull"):
		action_menu.find_node("Cull").set_pressed(true)
	if Input.is_action_pressed("ui_burn"):
		action_menu.find_node("Burn").set_pressed(true)
	if Input.is_action_pressed("ui_removerock"):
		action_menu.find_node("RemoveRock").set_pressed(true)
	if Input.is_action_pressed("ui_fertilize"):
		action_menu.find_node("Fertilize").set_pressed(true)

func _on_Help_pressed():
	get_node("Ui/Help").popup_centered()


func _on_MenuButton_pressed():
	get_node("Ui/Menu").popup_centered()

func _on_HexHover(hex, inside):
	#print("Hex ", hex, " at ", hex.pf_location)
	if inside:
		self.current_hex = hex
		hex.start_tooltip_timer()
		hex.get_node("Border").set_modulate(Color(0.0, 1.0, 0.0))
		self.update_current_hex_action_possibility()
	else:
		self.current_hex = null
		hex.update_tooltip("")
		hex.hide_tooltip()
		hex.get_node("Border").set_modulate(Color(1.0, 1.0, 1.0))
		self.update_current_hex_action_possibility()


func update_current_hex_action_possibility():
	if self.current_hex == null:
		# Reset mouse pointer
		Input.set_default_cursor_shape(Input.CURSOR_ARROW)
		return
	# Need to check if current action be done on the hex
	# Show user why not
	var action_compat = self.current_hex.can_do_action(self.current_action)
	var applicable = action_compat[0]
	var possible = action_compat[1]
	var reason = action_compat[2]
	var cost = action_compat[3]
	if cost > self.action_points:
		possible = false
		reason = "Too few action points. Have %d, need %d" % [self.action_points, cost]
	if applicable:
		if not possible:
			Input.set_default_cursor_shape(Input.CURSOR_FORBIDDEN)
			self.current_hex.get_node("Border").set_modulate(Color(1.0, 0.0, 0.0))
			self.current_hex.update_tooltip(reason)
		else:
			Input.set_default_cursor_shape(Input.CURSOR_CAN_DROP)
	else:
		self.current_hex.get_node("Border").set_modulate(Color(0.25, 0.25, 0.25))

func _on_HexInputEvent(viewport, event, shape, hex):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				self.hex_click_start = hex
			else:
				if self.hex_click_start == hex:
					self.do_action(hex, self.current_action)
				else:
					self.hex_click_start = null

func do_action(hex, action):
	# Recheck if action is possible on hex
	var action_compat = hex.can_do_action(action)
	var cost = action_compat[3]
	if not action_compat[0] or not action_compat[1]:
		return
	if cost > self.action_points:
		return
	print(action, " on ", hex)
	if action == "query":
		get_node("Ui/Right/VBoxContainer2/Details/Panel").set_text(hex.query_details())
		get_node("AudioQuery").play()
	elif action == "plant":
		print(self.current_plant)
		hex.do_action(action, 
		{"plant": self.plant_types[self.current_plant].duplicate()})
		get_node("AudioPlant").play()
	elif action == "cull":
		var r = hex.do_action(action, null)
		if r != null and r > 0:
			self.scores[self.current_year] += r
			print("Got %+d from culling plant", r)
			emit_signal("scores_updated", self.scores)
		get_node("AudioCull").play()
	elif action == "burn":
		hex.do_action(action, null)
		get_node("AudioBurn").play()
	elif action == "removerock":
		hex.do_action(action, null)
		get_node("AudioRemoveRock").play()
	elif action == "fertilize":
		hex.do_action(action, null)
		get_node("AudioFertilize").play()
	else:
		print("UNKNOWN ACTION ", action)
	if (cost > 0):
		var f = FadingText.instance()
		f.set_text("%+d AP" % -cost)
		f.set_position(Vector2(32, 0))
		f.move_towards = Vector2(64, -32)
		f.set_modulate(Color(0.9, 0.1, 0.1))
		hex.add_child(f)
	self.update_action_points(-cost)
		

func _on_action_changed(action):
	self.last_action = self.current_action
	self.current_action = action
	self.update_current_hex_action_possibility()
	if action == "pass_time":
		self.advance_time()
		self.stage_timer = self.stage_time

func _on_day_time_changed(old, new):
	get_node("Ui/Left/VBoxContainer/TimeLabel").set_text("Time: %s" % self.time_text[new])
	# Show a banner of the stage
	var banner = FadingText.instance()
	banner.set_text(self.time_text[old])
	banner.set_scale(Vector2(2.0, 2.0))
	var center = get_viewport().get_visible_rect().size / 2
	var size = banner.get_text_size()
	banner.set_position(Vector2(center.x - size.x/2.0, center.y*2.0 - 20 - size.y/2.0))
	add_child(banner)
	if new == TIME.DAWN:
		get_node("AudioDawn").play()
	
func _on_season_changed(old, new):
	get_node("Ui/Left/VBoxContainer/SeasonLabel").set_text("Season: %s" % self.season_text[new])

func _on_year_changed(old, new):
	get_node("Ui/Left/VBoxContainer/YearLabel").set_text("Year: %d" % self.year)

func _on_action_points_changed(old, new):
	self.update_current_hex_action_possibility()

func _on_plant_type_changed(value):
	self.current_plant = value

extends PopupPanel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Continue_pressed():
	self.set_visible(false)

func _on_Quit_pressed():
	get_tree().quit()
	
func _on_Help_pressed():
	self.set_visible(false)
	# Brittle, refers to the layout in "Game" scenetree
	get_node("../Help").set_visible(true)


func _on_Restart_pressed():
	get_tree().get_root().get_node("/root/Game").reset()
	self.set_visible(false)

func _on_scores_updated(scores):
	var total = 0
	var max_score = 0
	for x in scores:
		total += x
		max_score = max(max_score, x)
	get_node("VBoxContainer/Score").set_bbcode("[center]Score: %d[/center]" % total)
	var s = ""
	for x in range(scores.size()):
		s += "Year %d: %d\n" % [x, scores[x]]
	print(s)
	get_node("VBoxContainer/ScoreByYear").set_bbcode(s)

# LD54 : Da 'lil Patch

You only have a small space. Can you keep it cultivated and producing well as
the years go on?

## Tools

 * Debian Linux
 * Godot 3.5 (gdscript)
 * Art: GIMP, mypaint, inkscape, krita
 * Sound: Audacity

## External Assets

 * Font: Bitstream Vera Mono Nerd

## Licenses

 * Assets (excluding font): CC-BY-SA-4.0
 * Source code: GPLv3
